from pyfcm import FCMNotification

class FCM:
    def __init__(self):
        self.push_service = FCMNotification(api_key="AIzaSyD-k7DsM72bG6YJy7K19EMhVB1KYuHo_Iw")
    
    def send_notification_to_single_device(self, registration_id, message, room):
        data_message = {"room_id": room.id, "message": message}
        result = self.push_service.notify_single_device(registration_id=registration_id, data_message=data_message)
        print(result)

    def send_notification_to_multiple_devices(self, registration_ids, message, room):
        data_message = {"room_id": room.id, "message": message}
        result = self.push_service.notify_multiple_devices(registration_ids=registration_ids, data_message=data_message)
        print(result)