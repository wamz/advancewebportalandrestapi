import braintree

class BraintreeAPI:
    def __init__(self):
        braintree.Configuration.configure(
            braintree.Environment.Sandbox,
            merchant_id="qh9k5gsdqz3nc6p3",
            public_key="v6hcgxwhxqcqkcy9",
            private_key="204d6f7e69fc7128400edf4e4b0f6231"
        )
                                  
    @property
    def get_client_token(self):
        return braintree.ClientToken.generate()
    
    # get the payment method nonce sent by the android app and create a transaction that will be stored in the db