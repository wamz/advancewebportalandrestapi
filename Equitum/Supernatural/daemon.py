from threading import Thread

def summon_daemon(f):  
    def decorator(*args, **kwargs):
        # initialize thread
        t = Thread(target=f, args=args, kwargs=kwargs)    
        # This is the magic that must keep a thread alive       
        t.daemon = True     
        t.start()  
    return decorator