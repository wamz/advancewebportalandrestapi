import datetime

def get_today_date():
    return datetime.date.today()

def get_datetime_now():
    return datetime.datetime.now()

def get_seconds_left(today, date2):
    diff = date2 - today
    return diff.days * 24 * 3600 + diff.seconds

def convert_date_to_datetime(date):
    return datetime.datetime.combine(date, datetime.time())