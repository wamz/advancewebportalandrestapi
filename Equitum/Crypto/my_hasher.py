from django.contrib.auth.hashers import PBKDF2PasswordHasher as hasher
from random import choice
from string import *
import hashlib

def hash_device_key():
    h = hasher()
    salt = ''.join(choice(ascii_uppercase + digits + ascii_lowercase) for i in range(30))
    salt2 = ''.join(choice(ascii_uppercase + digits + ascii_lowercase) for i in range(30)).encode('utf-8')
    token = ''.join(choice(ascii_uppercase + digits + ascii_lowercase) for i in range(30))
    device_key = h.encode(token, salt)
    device_key_hash = hashlib.sha256(device_key.encode('utf-8') + salt2)
    return device_key_hash.hexdigest()