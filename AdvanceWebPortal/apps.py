from django.apps import AppConfig


class AdvancewebportalConfig(AppConfig):
    name = 'AdvanceWebPortal'
