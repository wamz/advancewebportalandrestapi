from rest_framework import serializers
from .models import *

class AppUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = AppUser
        fields = ('id', 'name', 'email', 'device_id')

class HotelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Hotel
        fields = ('id', 'name', 'address_line_1', 'address_line_2', 'town_city', 'county', 'postcode', 'country',
        'telephone_number', 'email')

class RoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = Room
        fields = ('id', 'room_number', 'room_name', 'room_size', 'room_description', 'valid_for_bid_from',
        'valid_for_bid_until', 'currency', 'starting_bid_price', 'hotel', 'leading_bid', 'main_room_image')

class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ('id', 'name', 'ext', 'room', 'room_image')

class BidSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bid
        fields = ('id', 'room', 'app_user', 'amount')