from .models import *
from sched import scheduler as Scheduler
from Equitum.FCM.FCM import FCM
from Equitum.Tempus.datetime import *
from Equitum.Supernatural.daemon import summon_daemon
import time
import datetime

class CountdownChecker:
    def __init__(self, reschedule):
        self.reschedule = reschedule
        self.refresh_interval = 1
        # setting up the Scheduler
        self.scheduler = Scheduler(time.time, time.sleep)
        self.scheduler.enter(0, 1, self.__check_rooms_countdown)
        self.scheduler.run()

    def __check_rooms_countdown(self):
        rooms = list(filter(lambda r: r.leading_bid != None and r.booked == False and r.valid_for_bid_until > get_today_date(), Room.objects.all()))
        for room in rooms:
            self.__check_room_countdown(room)

        if self.reschedule:
            self.scheduler.enter(self.refresh_interval, 1, self.__check_rooms_countdown)

    @summon_daemon
    def __check_room_countdown(self, room):
        seconds_left = get_seconds_left(get_datetime_now(), convert_date_to_datetime(room.valid_for_bid_until))
        print("{} has {} seconds left".format(room.room_name, seconds_left))

        if seconds_left == 86400:
            gcm_registration_ids = []
            for bid in Bid.objects.filter(room=room):
                gcm_registration_ids.append(bid.app_user.gcm_registration_id)

            message = "1 hour left to bid on room - {}".format(str(room))
            FCM().send_notification_to_multiple_devices(gcm_registration_ids, message, room)

        if seconds_left == 0:
            winning_bid = Bid.objects.filter(room=room).order_by('-amount')[:1][0]

            if room.leading_bid == winning_bid.amount:
                print("Winner : User - {}".format(str(winning_bid.app_user)))
                # send notification to winner
                gcm_registration_id = winning_bid.app_user.gcm_registration_id
                message = "You won the bid for room - {}\n Please complete booking for the room".format(str(room))
                FCM().send_notification_to_single_device(gcm_registration_id, message, room)
                # send notification to losing users
                gcm_registration_ids = []
                for losing_bid in Bid.objects.filter(room=room).exclude(app_user=winning_bid.app_user):
                    gcm_registration_ids.append(losing_bid.app_user.gcm_registration_id)
                    print("Loser : User - {}".format(str(losing_bid.app_user)))

                message_body = "You lost the bid for room - " + str(room)
                FCM().send_notification_to_multiple_devices(gcm_registration_ids, message, room)
