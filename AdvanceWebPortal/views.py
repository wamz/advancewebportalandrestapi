from django.shortcuts import render, get_object_or_404, get_list_or_404
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from .models import *
from .serializers import *
from django.contrib import auth
from django.contrib import messages
from django.template import RequestContext
from django.contrib.auth import login
from django.utils import timezone
from decimal import Decimal
from django.conf import settings
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from Equitum.Crypto.my_hasher import *
from Equitum.PaymentsAPI.braintree_api import BraintreeAPI
from Equitum.Tempus.datetime import *
from Equitum.FCM.FCM import FCM

import os
import shutil

# Create your views here.
# AdvanceWebPortal views

def get_index_page(request):
    return render(request, 'index.html')

def hotel_register(request):
    hotel_name = request.POST['hotel_name']
    town_city = request.POST['town_city']
    postcode = request.POST['postcode']
    country = request.POST['country']
    email = request.POST['email']
    password = request.POST['password']
    confirm_password = request.POST['confirm_password']

    if not hotel_name or not town_city or not postcode or not country or not email or not password or not confirm_password:
        data = {'hotel_name':hotel_name, 'town_city':town_city, 'postcode':postcode, 'country':country, 'email':email, 'password':password, 'confirm_password':confirm_password}
        messages.info(request, 'All Form Fields Are Required!')
        return render(request, 'index.html', data)
    
    try:
        if Hotel.objects.get(email=email):
            data = {'hotel_name':hotel_name, 'town_city':town_city, 'postcode':postcode, 'country':country, 'email':email, 'password':password, 'confirm_password':confirm_password}
            messages.info(request, 'Email Address Already Exists! \n Please Login')
            return render(request, 'index.html', data)
    except Hotel.DoesNotExist:
        pass

    if password == confirm_password:
        extra_fields = {}
        extra_fields["name"] = hotel_name
        extra_fields["town_city"] = town_city
        extra_fields["postcode"] = postcode
        extra_fields["country"] = country

        hotel = Hotel.objects.create_user(email, password, **extra_fields)
        return render(request, 'rooms.html', RequestContext(request, {'hotel' : hotel}))

    data = {'hotel_name':hotel_name, 'town_city':town_city, 'postcode':postcode, 'country':country, 'email':email, 'password':password, 'confirm_password':confirm_password}
    messages.info(request, 'Passwords Do Not Match!')
    return render(request, 'index.html', data)

def hotel_login(request):
    email = request.POST['email']
    password = request.POST['password']

    if not email or not password:
        messages.info(request, 'All Form Fields Are Required!')
        return render(request, 'index.html', {'email':email, 'password':password})

    hotel = auth.authenticate(username=email, password=password)

    if hotel is not None:
        auth.login(request, hotel)
        return HttpResponseRedirect('/rooms')

    messages.info(request, 'Invalid Login Details!')
    return render(request, 'index.html', {'email':email, 'password':password})

def logout(request):
    auth.logout(request)
    return HttpResponseRedirect('/')

def get_add_room_form(request):
    if request.user.is_authenticated():
        return render(request, 'add_room.html', {'hotel' : request.user})
    return HttpResponseRedirect('/')

def create_room(request):
    if request.user.is_authenticated():
        hotel = Hotel.objects.get(email=request.user)
        list_of_images = request.FILES.getlist('room_images')
        main_room_image = list_of_images[0]

        new_room = Room.objects.create(
            room_number = request.POST.get('room_number', ''),
            room_name = request.POST.get('room_name', ''),
            room_size = request.POST.get('room_size', ''),
            room_description = request.POST.get('room_description', ''),
            valid_for_bid_from = request.POST.get('valid_for_bid_from', timezone.now),
            valid_for_bid_until = request.POST.get('valid_for_bid_until', timezone.now),
            currency = request.POST.get('currency', ''),
            starting_bid_price = Decimal(request.POST.get('starting_price', 0.00)),
            hotel = hotel
        )

        room = Room.objects.get(pk=new_room.id)
        image_location(room, main_room_image)
        room.main_room_image = main_room_image
        room.save()

        for room_image in list_of_images:
            if room_image == main_room_image:
                continue

            image_name, image_ext = os.path.splitext(room_image.name)

            new_image = Image.objects.create(
                name = image_name,
                ext = image_ext,
                room = room
            )

            image = Image.objects.get(pk=new_image.id)
            upload_image(image, room_image)
            image.room_image = room_image
            image.save()

        return HttpResponseRedirect('/rooms')
    return HttpResponseRedirect('/')

def get_rooms(request):
    if request.user.is_authenticated():
        rooms = Room.objects.filter(hotel=request.user)
        return render(request, 'rooms.html', {'hotel': request.user, 'rooms': rooms})
    return HttpResponseRedirect('/')
    

def get_room_details(request, room_id):
    if request.user.is_authenticated():
        room = get_object_or_404(Room, pk=room_id)
        images = get_list_or_404(Image, room=room)
        return render(request, 'room_details.html', {'hotel': request.user, 'room': room, 'images': images})
    return HttpResponseRedirect('/')

def get_update_room_details_form(request, room_id):
    if request.user.is_authenticated():
        room = get_object_or_404(Room, pk=room_id)        
        return render(request, 'update_room_details.html', {'hotel': request.user, 'room': room})
    return HttpResponseRedirect('/')

def update_room_details(request, room_id):
    if request.user.is_authenticated():
        hotel = Hotel.objects.get(email=request.user)
        room = get_object_or_404(Room, pk=room_id)

        room.room_number = request.POST.get('room_number', '')
        room.room_name = request.POST.get('room_name', '')
        room.room_size = request.POST.get('room_size', '')
        room.room_description = request.POST.get('room_description', '')
        room.valid_for_bid_from = request.POST.get('valid_for_bid_from', timezone.now)
        room.valid_for_bid_until = request.POST.get('valid_for_bid_until', timezone.now)
        room.currency = request.POST.get('currency', '')
        room.starting_bid_price = request.POST.get('starting_bid_price', 0.00)
        room.save()

        return HttpResponseRedirect('/rooms')
    return HttpResponseRedirect('/')

def delete_room(request, room_id):
    if request.user.is_authenticated():
        room = get_object_or_404(Room, pk=room_id)
        shutil.rmtree(os.path.join(settings.MEDIA_ROOT, 'Room_Images', 'Room_'+str(room.id)))
        room.delete()

        return HttpResponseRedirect('/rooms')
    return HttpResponseRedirect('/')

def get_hotel_details(request):
    if request.user.is_authenticated():
        return render(request, 'hotel_details.html', {'hotel': request.user})
    return HttpResponseRedirect('/')

def get_update_hotel_details_form(request):
    if request.user.is_authenticated():
        return render(request, 'update_hotel_details.html', {'hotel': request.user})
    return HttpResponseRedirect('/')

def update_hotel_details(request):
    if request.user.is_authenticated():
        hotel = Hotel.objects.get(email=request.user)

        hotel.address_line_1 = request.POST.get('address_line_1', '')
        hotel.address_line_2 = request.POST.get('address_line_2', '')
        hotel.town_city = request.POST.get('town_city', '')
        hotel.county = request.POST.get('county', '')
        hotel.postcode = request.POST.get('postcode', '')
        hotel.telephone_number = request.POST.get('telephone_number', '')
        hotel.save()

        return HttpResponseRedirect('/hotel/details')
    return HttpResponseRedirect('/')

# RESTAPI Views
# url - fbAuthRegister/
class FacebookAuthRegister(APIView):
    def post(self, request):
        app_user = AppUser.objects.create(
            name=request.data["name"],
            email=request.data["email"],
            account_type=request.data["account_type"],
            device_id = hash_device_key(),
            gcm_registration_id=request.data["gcm_registration_id"]
        )

        return Response({'Message': 'Login Successful!', 'UserDetails': AppUserSerializer(app_user).data})

# url - fbAuthLogin/
class FacebookAuthLogin(APIView):
    def post(self, request):
        try:
            app_user = AppUser.objects.get(email=request.data["email"])

            if app_user.gcm_registration_id == request.data["gcm_registration_id"]:
                return Response({'Message': 'Login Successful!', 'UserDetails': AppUserSerializer(app_user).data})
            
            app_user.gcm_registration_id = request.data["gcm_registration_id"]
            app_user.save()
            return Response({'Message': 'Login Successful!', 'UserDetails': AppUserSerializer(app_user).data})
        except AppUser.DoesNotExist:
            return Response({'Message': 'Unauthorized User!'})

# url - search/
class SearchForRooms(APIView):
    def post(self, request):
        try:
            app_user = AppUser.objects.get(device_id=request.META.get('HTTP_AUTHORIZATION'))
            rooms = []
            images = []
            bids = []

            # get hotels in the searched city
            hotels = Hotel.objects.filter(town_city=request.data["town_city"])

            for hotel in hotels:
                # get rooms offered for bid by hotels
                rms = Room.objects.filter(hotel=hotel, valid_for_bid_from__lte=get_today_date(), valid_for_bid_until__gte=get_today_date()).filter(room_size=request.data["room_size"]).filter(booked=False)
                for room in rms:
                    rooms.append(room)

            for room in rooms:
                # get user's highest bid for each room they have bid on
                for bid in Bid.objects.filter(app_user=app_user).filter(room=room).order_by('-amount')[:1]:
                    bids.append(bid)

                # get images for room
                for image in Image.objects.filter(room=room):
                    images.append(image)
            
            content = {
                'Hotels': HotelSerializer(hotels, many=True).data,
                'Rooms': RoomSerializer(rooms, many=True).data,
                'Images': ImageSerializer(images, many=True).data,
                'Bids': BidSerializer(bids, many=True).data,
            }

            return Response(content)
        except AppUser.DoesNotExist:
            return Response({'Message': 'Unauthorized User!'})
        except (Bid.DoesNotExist, Room.DoesNotExist, Hotel.DoesNotExist, Image.DoesNotExist):
            pass

# URL - rooms/
class Rooms(APIView):
    def get(self, request, room_id):
        try:
            app_user = AppUser.objects.get(device_id=request.META.get('HTTP_AUTHORIZATION'))
            room = Room.objects.get(pk=room_id)
            hotel = room.hotel
            images = Image.objects.filter(room=room)
            bid = Bid.objects.filter(app_user=app_user).filter(room=room).order_by('-amount')[:1]           
            
            content = {
                'Hotel': HotelSerializer(hotel).data,
                'Room': RoomSerializer(room).data,
                'Images': ImageSerializer(images, many=True).data,
                'Bid': BidSerializer(bid, many=True).data,
            }

            return Response(content)
        except AppUser.DoesNotExist:
            return Response({'Message': 'Unauthorized User!'})
        except (Bid.DoesNotExist, Room.DoesNotExist, Hotel.DoesNotExist, Image.DoesNotExist):
            pass

# URL - bids/
class Bids(APIView):
    # place a bid
    def post(self, request):
        try:
            app_user = AppUser.objects.get(device_id=request.META.get('HTTP_AUTHORIZATION'))
            room = Room.objects.get(pk=request.data["room_id"])
            bid_amount = request.data["bid_amount"]

            try:
                bid = Bid.objects.filter(room=room).get(app_user=app_user)
                bid.amount = bid_amount
                bid.save()
                room.leading_bid = bid_amount
                room.save()
            except Bid.DoesNotExist:
                Bid.objects.create(room=room, app_user=app_user, amount=bid_amount)
                room.leading_bid = bid_amount
                room.save()

            # get gcm_registration_ids of all users who have outbidded on the room
            gcm_registration_ids = []
            for bid in Bid.objects.filter(room=room).exclude(app_user=app_user):
                gcm_registration_ids.append(bid.app_user.gcm_registration_id)
            
            message = "You are losing the bid for room - " + str(room)
            FCM().send_notification_to_multiple_devices(gcm_registration_ids, message, room)
            return Response({'Message': 'Bid placed successfully!'})
        except AppUser.DoesNotExist:
            return Response({'Message': 'Unauthorized User!'})
    
    # get list of bids
    def get(self, request):
        try:
            rooms = []
            images = []
            
            app_user = AppUser.objects.get(device_id=request.META.get('HTTP_AUTHORIZATION'))
            bids = Bid.objects.filter(app_user=app_user)

            for bid in bids:
                rooms.append(Room.objects.get(id=bid.room.id))
            
            for room in rooms:
                hotels = Hotel.objects.filter(room=room.hotel.id)

            for image in Image.objects.filter(room=room):
                images.append(image)

            content = {
                'Bids': BidSerializer(bids, many=True).data,
                'Rooms': RoomSerializer(rooms, many=True).data,
                'Hotels': HotelSerializer(hotels, many=True).data,
                'Images': ImageSerializer(images, many=True).data,
            }

            return Response(content)
        except AppUser.DoesNotExist:
            return Response({'Message': 'Unauthorized User!'})
        except (Bid.DoesNotExist, Room.DoesNotExist, Hotel.DoesNotExist, Image.DoesNotExist):
            pass

# URL - payments/
class Payments(APIView):
    # get client token
    def get(self, request):
        try:
            app_user = AppUser.objects.get(device_id=request.META.get('HTTP_AUTHORIZATION'))
            braintree = BraintreeAPI()
            return Response({"braintree_client_token": braintree.get_client_token})
        except AppUser.DoesNotExist:
            return Response({'Message': 'Unauthorized User!'})

    # create transaction
    def post(self, request):
        pass