from .models import Hotel

class HotelAuth(object):

    # check login details are valid
    def authenticate(self, username=None, password=None):
        try:
            hotel = Hotel.objects.get(email=username)

            if hotel.check_password(password):
                return hotel
        except Hotel.DoesNotExist:
            return None
    
    # get logged in user
    def get_user(self, hotel_id):
        try:
            hotel = Hotel.objects.get(pk=hotel_id)

            if hotel.is_active:
                return hotel
            
            return None
        except Hotel.DoesNotExist:
            return None