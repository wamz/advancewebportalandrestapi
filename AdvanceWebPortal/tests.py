from django.test import TestCase
from .models import *
from .Controllers.MyHasher import MyHasher

# Create your tests here.
# AdvancedWebPortal Unit Tests

class HotelUnitTests(TestCase):
    def setUp(self):
        pass

    def test_create_hotel(self):
        pass

class RoomUnitTests(TestCase):
    def setUp(self):
        Hotel.objects.create(
            name="A Hotel",
            town_city="Cambridge",
            postcode="AB12CD",
            country="UK",
            email="a@hotel.com",
            password="ahotel"
        )

    def test_create_room(self):
        hotel = Hotel.objects.get(email="a@hotel")

        Room.objects.create(
            room_number="42",
            room_name="The First Room",
            room_size="Single Room",
            room_description="A room perfect for one.",
            valid_for_bid_from="2016-08-01",
            valid_for_bid_until="2016-08-30",
            currency="British Pound",
            starting_bid_price="29.99",
            hotel=hotel
        )

        self.assertEqual(Room.objects.get(pk=1).room_name, "The First Room")

    def test_update_room(self):
        room = Room.objects.get(pk=1)

        room.room_size = "Double Room"
        room.room_description = "This room is perfect for a loved up couple."
        room.save()

        self.assertEqual(Room.objects.get(pk=1).room_size, "Double Room")
        self.assertEqual(Room.objects.get(pk=1).room_description, "This room is perfect for a loved up couple.")

    def test_delete_room(self):
        Room.objects.get(pk=1).delete()

        self.assertEqual(Room.objects.get(pk=1).count(), 0)

class ImageUnitTests(TestCase):
    def setUp(self):
        pass

    def test_create_image(self):
        pass

    def test_delete_image(self):
        pass

class AppUserUnitTests(TestCase):
    def setUp(self):
        my_hasher = MyHasher()

        AppUser.objects.create(
            name="Michael Knight",
            email="michael@knight.com",
            account_type="facebook",
            device_id = my_hasher.hash_device_key(),
            gcm_registration_id="erkjou34jorenlksnlka"
        )

    def test_create_user(self):
        my_user = AppUser.objects.get(email="michael@knight.com")
        self.assertEqual(my_user.email, "michael@knight.com")
    
    def test_update_user(self):
        my_user = AppUser.objects.get(email="michael@knight.com")
        my_user.gcm_registration_id = "34ohio34jpnsdaiwioigiwwoh9"
        my_user.save()

        user = AppUser.objects.get(email="michael@knight.com")
        self.assertEqual(user.gcm_registration_id, "34ohio34jpnsdaiwioigiwwoh9")

class BidUnitTests(TestCase):
    def setUp(self):
        pass

    def test_create_bid(self):
        pass