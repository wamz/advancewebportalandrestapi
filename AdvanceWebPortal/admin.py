from django.contrib import admin
from .models import *

# Register your models here.
# AdvancedWebPortal
admin.site.register(Hotel)
admin.site.register(Room)
admin.site.register(Image)
admin.site.register(AppUser)
admin.site.register(Bid)
admin.site.register(Booking)
