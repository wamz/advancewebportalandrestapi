# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-08-28 16:25
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('AdvanceWebPortal', '0015_auto_20160819_0149'),
    ]

    operations = [
        migrations.AddField(
            model_name='room',
            name='booked',
            field=models.BooleanField(default=False),
        ),
    ]
