from django.db import models
from django.utils import timezone
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.utils.translation import ugettext_lazy as _

# Create your models here.
# AdvanceWebPortal models

class CustomUserManager(BaseUserManager):
    def _create_user(self, email, password, is_staff, is_superuser, **extra_fields):
        # This is a private function
        # It is going to be a reusable function used to create both admin users and normal users using email for auth

        if not email:
            raise ValueError("The email provided must not be empty!")
        
        # checks email is valid
        email = self.normalize_email(email)
        user = self.model(
            email = email,
            is_active = True,
            is_staff = is_staff,
            is_superuser = is_superuser,
            last_login = timezone.now(),
            date_joined = timezone.now(),
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self._db)
        return user
    
    def create_user(self, email, password, **extra_fields):
        return self._create_user(email, password, False, False, **extra_fields)
    
    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, True, **extra_fields)

class Hotel(AbstractBaseUser, PermissionsMixin):
    name = models.CharField(max_length=254, blank=True)
    address_line_1 = models.CharField(max_length=1024, blank=True, null=True)
    address_line_2 = models.CharField(max_length=1024, blank=True, null=True)
    town_city = models.CharField(max_length=1024)
    county = models.CharField(max_length=1024, blank=True, null=True)
    postcode = models.CharField(max_length=20)
    country = models.CharField(max_length=1024)
    telephone_number = models.CharField(max_length=20, blank=True, null=True)
    email = models.EmailField(max_length=255, unique=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    date_joined = models.DateTimeField(default=timezone.now)

    # setting that email is to be used as the unique identifier for a user
    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ['name',]

    objects = CustomUserManager()

    class Meta:
        verbose_name = _('hotel')
        verbose_name_plural = _('hotels')


    def get_full_name(self):
        return self.name

    def get_short_name(self):
        return self.name

def image_location(room, file):
    return '{0}/Room_{1}/{2}'.format('Room_Images', room.id, file)

def upload_image(instance, file):
    return '{0}/Room_{1}/{2}'.format('Room_Images', instance.room.id, file)

class Room(models.Model):
    room_number = models.IntegerField()
    room_name = models.CharField(max_length=1024)
    room_size = models.CharField(max_length=100)
    room_description = models.TextField()
    valid_for_bid_from = models.DateField(auto_now=False, auto_now_add=False)
    valid_for_bid_until = models.DateField(auto_now=False, auto_now_add=False)
    currency = models.CharField(max_length=100)
    starting_bid_price = models.DecimalField(max_digits=10, decimal_places=2)
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE)
    leading_bid = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    main_room_image = models.FileField(upload_to=image_location, null=True, blank=True)
    booked = models.BooleanField(default=False)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(default=timezone.now)

    @property
    def get_images(self):
        return Image.objects.get(room=self)

    def __str__(self):
        return "{} - {}".format(str(self.room_number), self.room_name)

class Image(models.Model):
    name = models.CharField(max_length=1024)
    ext = models.CharField(max_length=100, null=True, blank=True)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    room_image = models.FileField(upload_to=upload_image, null=True, blank=True)
    created_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return "{}{}".format(self.name, self.ext)

class AppUser(models.Model):
    name = models.CharField(max_length=255)
    email = models.EmailField(max_length=255, unique=True)
    account_type = models.CharField(max_length=30)
    device_id = models.CharField(max_length=254, unique=True)
    gcm_registration_id = models.CharField(max_length=254, unique=True)

    def __str__(self):
        return "{} - {}".format(self.name, self.email)

class Bid(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    app_user = models.ForeignKey(AppUser, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return "{} bid for {} - {}".format(self.app_user.name, self.room.room_name, str(self.room.room_number))

class Booking(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    app_user = models.ForeignKey(AppUser, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    check_in_date = models.DateField(auto_now=False, auto_now_add=False)
    check_out_date = models.DateField(auto_now=False, auto_now_add=False)
    valid_for_bid_again = models.BooleanField(default=False)
    created_at = models.DateField(default=timezone.now)

    def __str__(self):
        return "{} booked by {} for {}-{}".format(self.room, self.app_user, self.room.currency, self.amount)