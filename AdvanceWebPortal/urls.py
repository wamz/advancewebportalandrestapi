from django.conf.urls import url
from . import views
from rest_framework.urlpatterns import format_suffix_patterns

app_name = "AdvanceWebPortal"
urlpatterns = [
    url(r'^$', views.get_index_page, name='get_index_page'),
    # auth urls
    url(r'^login/$', views.hotel_login, name='hotel_login'),
    url(r'^register/$', views.hotel_register, name='hotel_register'),
    url(r'^logout/$', views.logout, name='logout'),
    # room urls
    url(r'^rooms/$', views.get_rooms, name='get_rooms'),
    url(r'^add/room/$', views.get_add_room_form, name='get_add_room_form'),
    url(r'^create/room/$', views.create_room, name='create_room'),
    url(r'^room/details/(?P<room_id>[0-9]+)/$', views.get_room_details, name='get_room_details'),
    url(r'^room/update/(?P<room_id>[0-9]+)/$', views.get_update_room_details_form, name='get_update_room_details_form'),
    url(r'^update/room/(?P<room_id>[0-9]+)/$', views.update_room_details, name='update_room_details'),
    url(r'^delete/room/(?P<room_id>[0-9]+)/$', views.delete_room, name='delete_room'),
    # hotel urls
    url(r'^hotel/details/$', views.get_hotel_details, name='get_hotel_details'),
    url(r'^hotel/update/$', views.get_update_hotel_details_form, name='get_update_hotel_details_form'),
    url(r'^update/hotel/$', views.update_hotel_details, name='update_hotel_details'),

    #RESTAPI URLs
    url(r'^fbAuthRegister/', views.FacebookAuthRegister.as_view()),
    url(r'^fbAuthLogin/', views.FacebookAuthLogin.as_view()),
    url(r'^search/', views.SearchForRooms.as_view()),
    url(r'^rooms/(?P<room_id>[0-9]+)/', views.Rooms.as_view()),
    url(r'^bids/', views.Bids.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)