"""
WSGI config for AdvanceWeb project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "AdvanceWeb.settings")

application = get_wsgi_application()

from Equitum.Supernatural.daemon import summon_daemon
from AdvanceWebPortal.countdownchecker import CountdownChecker

@summon_daemon
def countdown_checker():
    CountdownChecker(True)

countdown_checker()