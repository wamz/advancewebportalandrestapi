Windows Installation:

Install the latest Python (3.X).

Install a git client of some description.

Ensure that Python and pip are available in your $PATH.

Using pip install python-virtualenv.

Create a new virtual environment to hold the project (virtualenv yog-sothoth (for instance)).

Clone the shoggoth package to the virtual env site packages folder (https://bitbucket.com/niadh/shoggoth).

Enter the virtual environment (cd yog-sothoth; bin/activate.bat).

Clone the git repo into the virtual environment.

Enter the project directory (cd [cloned git-repo name]).#

Install dependencies (pip install -r requirements.txt).

Migrate DB (python manage.py migrate).

Create superuser (python manage.py createsuperuser).

Start Django (python manage.py runserver 0.0.0.0:8000).

Access via http://localhost:8000.

Done.

Linux:

Linux is significantly easier to build on, however we don't use Linux servers...

The instructions above should still apply however, Python, pip and git should already come as default and be present in the system $PATH.